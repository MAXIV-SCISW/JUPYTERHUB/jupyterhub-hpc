###############################################################################
# Modified for MAX IV installation - should hopefully work elsewhere also.
###############################################################################
# Original makefile notes:
#
#     Automation for setting up JupyterHub on a HPC cluster.
#
#     This Makefile is a hack, and actually serves the role of a piecewise
#     shell script which does the setup and documents some important
#     management commands.  Not all pieces necessarily work - please
#     understand everything before you run it.
#
#     The install-related targes *should* mostly work and generally should
#     be general work, and we should try to make them work for others,
#     too.  Install-related targets should be idempotent (but sometimes if
#     re-run, they won't do something that is needed).
###############################################################################

# Makefile stuff
.DEFAULT_GOAL := help
SHELL := /bin/bash
.ONESHELL:
THIS_FILE := $(lastword $(MAKEFILE_LIST))

# Set the umask so that users have access to kernels and conda environments
UMASK := umask 002

# Colors
LIGHTPURPLE := \033[1;35m
GREEN := \033[32m
CYAN := \033[36m
BLUE := \033[34m
RED := \033[31m
NC := \033[0m

# Separator between output, 80 characters wide
define print_separator
    printf "$1"; printf "%0.s*" {1..80}; printf "$(NC)\n"
endef
print_line_green = $(call print_separator,$(GREEN)) 
print_line_blue = $(call print_separator,$(BLUE))
print_line_red = $(call print_separator,$(RED)) 


###############################################################################
##@ Help
###############################################################################

.PHONY: help

help:  ## Display this help message
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)Jupyter Docker Stacks $(CYAN)Makefile$(NC)\n"
	printf "    This makefile has been Modified for the MAX IV installation,\n"
	printf "    it should hopefully work elsewhere also.\n"
	printf "\n"
	printf "    The installation has been split into two parts, assumed to \n"
	printf "    be installed in two different locations, though they could\n"
	printf "    be installed on the same machine:\n"
	printf "    	JupyterHub\n"
	printf "    	JupyterLab\n"
	$(print_line_blue)
	printf "\n"
	$(print_line_blue)
	printf "$(BLUE)JupyterHub Installation Steps:$(NC)\n"
	printf "    $(CYAN)make $(NC)conda$(NC)\n"
	printf "    $(CYAN)make $(NC)hub-stable-env$(NC)\n"
	printf "\n"
	printf "$(BLUE)JupyterLab Installation Steps:$(NC)\n"
	printf "    $(CYAN)make $(NC)conda$(NC)\n"
	printf "    $(CYAN)make $(NC)lab-stable-env$(NC)\n"
	printf "    $(CYAN)make $(NC)link-kernels-stable$(NC)\n"
	printf "\n"
	printf "$(BLUE)Edit Configuration File:$(NC)\n"
	printf "    $(NC)jupyterhub_config.py$(NC)\n"
	printf "    $(NC)--> see README.md$(NC)\n"
	printf "\n"
	printf "$(BLUE)Run then in debug mode:$(NC)\n"
	printf "    $(CYAN)make $(NC)run-dev-debug-online$(NC)\n"
	printf "\n"
	printf "$(BLUE)Run then in live mode:$(NC)\n"
	printf "    $(CYAN)make $(NC)run-live-online$(NC)\n"
	printf "\n"
	printf "$(BLUE)Setup system service:$(NC)\n"
	printf "    $(NC)--> see README.md$(NC)\n"
	printf "\n"
	printf "$(BLUE)Run as a system service:$(NC)\n"
	printf "    $(CYAN)make $(NC)start$(NC)\n"
	printf "    $(CYAN)make $(NC)stop$(NC)\n"
	printf "    $(CYAN)make $(NC)restart$(NC)\n"
	printf "    $(CYAN)make $(NC)status$(NC)\n"
	printf "    $(CYAN)make $(NC)logs$(NC)\n"
	$(print_line_blue)
	printf "\n"
	$(print_line_blue)
	printf "$(BLUE)Usage\n    $(CYAN)make $(NC)<target>\n"
	awk 'BEGIN {FS = ":.*##";} /^[a-zA-Z_-].*##/ \
	{ printf "    $(CYAN)%-25s$(NC) %s\n", $$1, $$2} /^##@/ \
	{ printf "\n$(BLUE)%s$(NC)\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
	$(print_line_blue)


###############################################################################
##@ Operate JupyterHub As A Service
###############################################################################

.PHONY: start stop restart status logs

start: ## Start JupyterHub system service
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)JupyterHub Live Service $(CYAN)Starting$(NC)\n"
	$(print_line_blue)
	printf "\n"

	sudo systemctl start jupyterhub.service

	$(print_line_blue)
	printf "$(GREEN)Done $(CYAN)Starting$(NC)\n"
	$(print_line_blue)
	printf "\n"

stop: ## Stop JupyterHub system service
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)JupyterHub Live Service $(CYAN)Stopping$(NC)\n"
	$(print_line_blue)
	printf "\n"

	sudo systemctl stop jupyterhub.service

	$(print_line_blue)
	printf "$(GREEN)Done $(CYAN)Stopping$(NC)\n"
	$(print_line_blue)
	printf "\n"

restart: ## Restart JupyterHub system service
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)JupyterHub Live Service $(CYAN)Restarting$(NC)\n"
	$(print_line_blue)
	printf "\n"

	sudo systemctl restart jupyterhub.service 

	$(print_line_blue)
	printf "$(GREEN)Done $(CYAN)Restarting$(NC)\n"
	$(print_line_blue)
	printf "\n"

status: ## Status of JupyterHub system service
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)JupyterHub Live Service $(CYAN)Status$(NC)\n"
	$(print_line_blue)
	printf "\n"

	sudo systemctl status jupyterhub.service

	$(print_line_blue)
	printf "$(GREEN)Done $(CYAN)Status$(NC)\n"
	$(print_line_blue)
	printf "\n"

logs: ## View JupyterHub system service logs 
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)JupyterHub Live Service $(CYAN)Logs$(NC)\n"
	$(print_line_blue)
	printf "\n"

	sudo journalctl -u jupyterhub.service -f

	$(print_line_blue)
	printf "$(GREEN)Done $(CYAN)Logs$(NC)\n"
	$(print_line_blue)
	printf "\n"


###############################################################################
##@ Install (mini)conda
###############################################################################

# Define the name of the virtual environment directory
VENV := conda

# Installation of conda & required modules
$(VENV)/bin/activate:
	@printf "\n"

	# Conda environment creation - using mamba
	miniconda_installer=Miniforge3-Linux-x86_64.sh
	miniconda_url=https://artifactory.maxiv.lu.se/artifactory/kits/scisw/mamba/$$miniconda_installer

	$(print_line_blue)
	printf "\n$(BLUE)Downloading conda from $$miniconda_url$(NC)\n"
	wget $$miniconda_url

	printf "\n$(BLUE)Installing conda from $$miniconda_installer$(NC)\n"
	sh $$miniconda_installer -s -p $(VENV) -b
	rm $$miniconda_installer

	# Set conda repositories
	cp install_config/condarc $(VENV)/.condarc

	# Clean-up
	$(VENV)/bin/mamba clean --all --yes

	printf "\n$(GREEN)DONE INSTALLING VENV$(NC)\n"
	printf "  activate environment:    source $(VENV)/bin/activate \n"
	printf "  deactivate environment:  conda deactivate\n"
	printf "  remove environment:      make clean-venv\n"
	$(print_line_green)
	printf "\n"

conda: $(VENV)/bin/activate ## Install conda - miniconda with mamba installed

# Create JupyterHub conda environment, remove if it exists
$(VENV)/envs/hub-%/bin/jupyterhub: $(VENV)/bin/activate install_config/conda-env-hub-%.yml
	# Remove the environment - this ensures a clean installation
	$(MAKE) -s -f $(THIS_FILE) clean-hub-$*

	# When on the blue network, need to set pip module locations
	export PIP_INDEX_URL=https://artifactory.maxiv.lu.se/artifactory/api/pypi/pypi-virtual-stable/simple

	printf "Building environment: hub-$* \n"
	$(VENV)/bin/mamba env create -n hub-$* -f install_config/conda-env-hub-$*.yml

	# Clean-up
	$(VENV)/bin/mamba clean --all --yes

# Create JupyterLab conda environment, remove if it exists
$(VENV)/envs/lab-%/bin/jupyter-labhub: $(VENV)/bin/activate install_config/conda-env-lab-%.yml
	# Remove the environment - this ensures a clean installation
	$(MAKE) -s -f $(THIS_FILE) clean-lab-$*

	# Issues with installing extensions from local gitlab, setting PATH fixes this
	export PATH=$(PWD)/$(VENV)/envs/lab-$*/bin:$$PATH
	printf "PATH: $$PATH\n"

	printf "Building environment: lab-$* \n"
	$(VENV)/bin/mamba env create -n lab-$* -f install_config/conda-env-lab-$*.yml

	# Clean-up
	$(VENV)/bin/mamba clean --all --yes

	# Copy the overrides file - some default settings for extensions
	mkdir -p $(VENV)/envs/lab-$*/share/jupyter/lab/settings
	cp install_config/overrides.json $(VENV)/envs/lab-$*/share/jupyter/lab/settings/.

	# Link kernel images
	$(MAKE) -s -f $(THIS_FILE) link-kernels-$*


###############################################################################
##@ Install JupyterHub & Dependencies
###############################################################################

hub-stable-env: $(VENV)/envs/hub-stable/bin/jupyterhub ## Create hub stable conda environment

###############################################################################
##@ Install JupyterLab & Extensions & Dependencies
###############################################################################

lab-stable-env: $(VENV)/envs/lab-stable/bin/jupyter-labhub ## Create lab stable conda environment

link-kernels-lab-%:
	@$(UMASK)
	ln -sf /sw/jupyterhub/jupyter-kernels-apptainer/kernel-spec-files/share/jupyter/kernels/maxiv* \
		$(VENV)/envs/lab-$*/share/jupyter/kernels/.
	tree $(VENV)/envs/lab-$*/share/jupyter/kernels/

link-kernels-stable: link-kernels-lab-stable ## Link to kernel images in jupyter-kernels-apptainer


###############################################################################
##@ Development - JupyterHub Core & Extensions
###############################################################################

hub-dev-env: $(VENV)/envs/hub-dev/bin/jupyterhub ## Create hub dev conda environment

export-hub-dev-env: install_config/conda-env-hub-stable.yml ## Export the dev conda environment of the hub to yaml file 
	@printf "\n"
	printf "$(CYAN)To force rebuild of conda-env-hub-stable.yml:$(NC)\n"
	printf "    rm install_config/conda-env-hub-stable.yml \n"
	printf "    make export-hub-dev-env \n\n"

install_config/conda-env-%-stable.yml:
	@$(UMASK)
	@printf "\n"
	$(print_line_blue)
	printf "Exporting environment: $* \n"
	$(print_line_blue)

	# Export base conda environment
	export_file=$(PWD)/install_config/conda-env-$*-stable.yml
	printf "$(LIGHTPURPLE)saving to:\n$(NC)  $$export_file \n"
	$(PWD)/$(VENV)/bin/mamba env export -n $*-dev --no-builds --file "$$export_file" 

	$(print_line_blue)
	printf "\n"

clean-conda: ## Remove conda environment
	rm -rf $(VENV)
clean-hub-dev: ## Remove hub dev environment
	printf "\nRemoving environment: hub-dev \n"
	rm -rf $(VENV)/envs/hub-dev
clean-hub-stable: ## Remove hub dev environment
	printf "\nRemoving environment: hub-stable \n"
	rm -rf $(VENV)/envs/hub-stable


###############################################################################
##@ Development - Run JupyterHub Directly In Terminal
###############################################################################

.PHONY: run run-dev-debug-online run-dev-debug-offline

# Check if the jupyterhub executable exists, then run it
run-dev-debug-online: $(VENV)/envs/hub-dev/bin/jupyterhub ## Run JupyterHub dev version, debug mode
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)Starting JupyterHub $(CYAN)in debug mode:$(NC)\n"
	source $(PWD)/$(VENV)/bin/activate
	source activate $(VENV)/envs/hub-dev
	export DEV=1
	export LOCATION=online
	jupyterhub -f jupyterhub_config.py --debug
	$(print_line_blue)

run-dev-debug-offline: ## Run JupyterHub dev version, debug mode
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)Starting JupyterHub $(CYAN)in debug mode:$(NC)\n"
	source $(PWD)/$(VENV)/bin/activate
	export DEV=1
	export LOCATION=offline
	jupyterhub -f jupyterhub_config.py --debug
	$(print_line_blue)

# Check if the jupyterhub executable exists, then run it
run-live-online: $(VENV)/envs/hub-stable/bin/jupyterhub ## Run JupyterHub live version, output in terminal
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)Starting JupyterHub $(CYAN)in live mode:$(NC)\n"
	source $(PWD)/$(VENV)/bin/activate
	source activate $(VENV)/envs/hub-stable
	export LOCATION=online
	jupyterhub -f jupyterhub_config.py
	$(print_line_blue)

run-live-offline: ## Run JupyterHub live version, output in terminal
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)Starting JupyterHub $(CYAN)in live mode:$(NC)\n"
	source $(PWD)/$(VENV)/bin/activate
	export LOCATION=offline
	jupyterhub -f jupyterhub_config.py
	$(print_line_blue)


###############################################################################
##@ Development - JupyterLab & Extensions & Dependencies
###############################################################################

lab-dev-env: $(VENV)/envs/lab-dev/bin/jupyter-labhub ## Create lab dev conda environment

link-kernels-dev: link-kernels-lab-dev ## Links to kernel images in jupyter-kernels-apptainer

export-lab-dev-env: install_config/conda-env-lab-stable.yml ## Export the dev conda environment of the lab to yaml file
	@printf "\n"
	printf "$(CYAN)To force rebuild of conda-env-lab-stable.yml:$(NC)\n"
	printf "    rm install_config/conda-env-lab-stable.yml \n"
	printf "    make export-lab-dev-env \n\n"
clean-lab-dev: ## Remove lab dev environment
	printf "\nRemoving environment: lab-dev \n"
	rm -rf $(VENV)/envs/lab-dev
clean-lab-stable: ## Remove lab dev environment
	printf "\nRemoving environment: lab-stable \n"
	rm -rf $(VENV)/envs/lab-stable

###############################################################################
##@ Development - Lmod Kernels (no longer used)
###############################################################################

# Install development version of a kernel
kernel-dev/%: ## Install kernel from development conda yaml file 
	@$(UMASK)
	$(call install_kernel,$(notdir $@),"development")

define install_kernel
	@printf "\n"
	$(print_line_blue)
	kernel_name=$(1)
	version=$(2)
	printf "$(BLUE)Installing $(CYAN)$$version $(NC)$(BLUE)kernel "
	printf "$(CYAN)$$kernel_name $(NC)\n"
	$(print_line_blue)

	# Source the base conda environemnt 
	source $(PWD)/$(VENV)/bin/activate

	# Get information for a given kernel name from the config file
	output=$$(python3 kernels_install/read_kernel_config.py \
		--get_kernel_info $$kernel_name 2>&1)
	eval "arr=($$output)"

	kernel_name=$${arr[0]} 
	kernel_disp_name=$${arr[1]}
	conda_env_name=$${arr[2]}
	conda_file_stable=$${arr[3]}
	conda_file_development=$${arr[4]}
	module_list=$${arr[5]}
	env_vars=$${arr[6]}

	if [[ "$$version" = "stable" ]]  
	then
		conda_file=$$conda_file_stable
	elif [[ "$$version" = "development" ]]  
	then
		conda_file=$$conda_file_development
		kernel_name+="_development"
		conda_env_name+="_development"
		kernel_disp_name+=" / Development"
	else
		conda_file=$$conda_file_stable
		kernel_name+="_validation"
		conda_env_name+="_validation"
		kernel_disp_name+=" / Validation"
	fi

	# If a conda environment needs to be created, do so
	if [[ "$$conda_file" != "none" ]]
	then
		bash kernels_install/create_conda_env.sh "$$kernel_name" \
			"$$conda_env_name" "$$conda_file" "$$module_list" 
	fi
	
	# Install the kernel
	bash kernels_install/install_conda_or_lmod_kernel.sh "$$kernel_name" \
		"$$kernel_disp_name" "$$conda_env_name" "$$conda_file" \
		"$$module_list" "$$env_vars"

	$(print_line_blue)
	printf "$(GREEN)Done $(NC)$(BLUE)installing the kernel "
	printf "$(CYAN)$$kernel_name $(NC)\n"
	$(print_line_blue)
	printf "\n"
endef


# Save pinned version of every module installed in a conda environment
export-kernel-dev/%: ## Export full conda environment of kernel to yaml file 
	@$(UMASK)
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)Exporting list of development version of "
	printf "$(CYAN)$(notdir $@) modules$(NC)\n"
	$(print_line_blue)

	# Source the base conda environemnt
	source $(PWD)/$(VENV)/bin/activate
	KERNEL_PREFIX=$$CONDA_PREFIX

	# Get information for a given kernel name from the config file
	kernel_name=$(notdir $@)
	output=$$(python3 kernels_install/read_kernel_config.py \
		--get_kernel_info $$kernel_name 2>&1)
	eval "arr=($$output)"

	kernel_name_stable=$${arr[0]} 
	kernel_disp_name=$${arr[1]}
	conda_env_name=$${arr[2]}
	conda_file_stable=$${arr[3]}
	conda_file_development=$${arr[4]}
	module_list=$${arr[5]}

	# Development versions have same appended description
	kernel_dev_name="$$kernel_name_stable""_development"
	conda_env_name_dev="$$conda_env_name""_development"

	printf " $(CYAN)          kernel name: $(NC)$$kernel_name_stable$(NC)\n"
	printf " $(CYAN)     development name: $(NC)$$kernel_dev_name$(NC)\n"
	printf " $(CYAN)     kernel_disp_name: $(NC)$$kernel_disp_name$(NC)\n"
	printf " $(CYAN)       conda_env_name: $(NC)$$conda_env_name$(NC)\n"
	printf " $(CYAN)   conda_env_name_dev: $(NC)$$conda_env_name_dev$(NC)\n"
	printf " $(CYAN)conda_file_development: $(NC)$$conda_file_development$(NC)\n"
	printf " $(CYAN)     conda_file_stable: $(NC)$$conda_file_stable$(NC)\n"
	printf " $(CYAN)          module_list: $(NC)$$module_list$(NC)\n"

	# See if the development conda environment exists
	if [[ "$$conda_file_development" != "none" ]]
	then
		conda_env_dir=$$KERNEL_PREFIX/share/jupyter/conda_envs/$$conda_env_name_dev
		if [ -d "$$conda_env_dir" ]
		then
			printf "\n"
			printf "The development conda environment exists:\n"
			printf "    $(CYAN)$$conda_env_dir $(NC)\n"
			printf "\n"

			export_file=$(PWD)/$$conda_file_stable
			printf "Exporting development conda environment to stable "
			printf "config file location:\n    $(CYAN)$$export_file $(NC)\n"

			# Export the kernel's conda environment
			source activate $$conda_env_dir
			mamba env export --no-builds --file "$$export_file" 

			printf "Any packages installed via git repositories will need to "
			printf "be edited properly in the resulting file\n\n"
			printf "The kernel can now be created with the stable conda config"
			printf " file with:\n $(GREEN)"
			printf "    make kernel-stable/$$kernel_name_stable$(NC)\n"
		else
			printf "No conda environment exists for this kernel\n"
		fi
	else
		printf "No conda environment has been defined for this kernel\n"
	fi


	$(print_line_blue)
	printf "\n"
