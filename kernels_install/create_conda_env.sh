#!/bin/bash
#
# Description: This is a script for creating conda environments, given a few
#              parameters:
#                   conda_env_name="$1"
#                   conda_file="$2"
#                   module_list="$3"

# Colors
LIGHTPURPLE="\033[1;35m"
GREEN="\033[32m"
BLUE="\033[34m"
CYAN="\033[1;36m"
RED="\033[31m"
NC="\033[0m"

# Separator between output, fills width of terminal
function print_separator
{
printf "${NC}$1%*s${NC}\n" "${COLUMNS:-80}" '' | tr ' ' '*'
}

printf "\n"
print_separator ${BLUE}

# Check input
if [[ "$#" -lt 2 ]]; then
    printf "${RED} Not enough input arguments ${NC}\n"
    print_separator ${BLUE}
    printf "\n"
    exit 0
fi

# Get input
kernel_name="$1"
conda_env_name="$2"
conda_file="$3"
module_list="$4"

# Module list is optional input, but it needs to be set to something
if [ -z "$module_list" ]
then
    module_list="none"
fi

# The procedure is slightly different if lmod modules are needed
if [[ "$module_list" = "none" ]]
then
    kernel_type="conda"
else
    kernel_type="lmod_conda"
fi

# Some special installation stpes are needed for Matlab kernels
matlab_kernel=false
if [[ $module_list == *"Matlab"* || $module_list == *"matlab"* ]]; then
    matlab_kernel=true
fi

# Source the base conda environemnt, set KERNEL_PREFIX path
source $PWD/miniconda/bin/activate $PWD/miniconda
KERNEL_PREFIX=$CONDA_PREFIX

# Set location of the conda environment that will be created
mkdir -p $KERNEL_PREFIX/share/jupyter/conda_envs
conda_env_dir=$KERNEL_PREFIX/share/jupyter/conda_envs/$conda_env_name

# See if the environment already exists, if so, leave it alone and exit
if [ -d "$conda_env_dir" ]
then
    printf "    ${LIGHTPURPLE}The conda environment ${CYAN}$conda_env_name "
    printf "${NC}${LIGHTPURPLE}already exists here:${NC}\n"
    printf "      $conda_env_dir\n"
    printf "    ${LIGHTPURPLE}To remove it and the corresponding kernel:${NC}\n"
    printf "        make clean-kernel-stable/$kernel_name\n"
    printf "    ${LIGHTPURPLE}exiting with no changes made${NC}\n"

else

    # Print what's going to happen
    printf "  ${BLUE}Creating a conda environment "
    if [[ "$kernel_type" = "lmod_conda" ]]
    then
        printf " that depends on lmod modules being loaded"
    fi
    printf "${NC}\n"
    printf "    ${CYAN}  kernel name: ${NC}$kernel_name${NC}\n"
    printf "    ${CYAN}   conda name: ${NC}$conda_env_name${NC}\n"
    printf "    ${CYAN}  kernel type: ${NC}$kernel_type${NC}\n"
    printf "    ${CYAN}    yaml file: ${NC}$conda_file${NC}\n"
    printf "    ${CYAN}  module_list: ${NC}$module_list${NC}\n"
    printf "    ${CYAN}    conda env: ${NC}$conda_env_dir${NC}\n"
    printf "\n"

    # Load lmod modules, if needed
    if [[ "$kernel_type" = "lmod_conda" ]]
    then
        printf "    ${BLUE}Loading lmod modules${NC}\n"

        ml purge
        ml load $module_list
        ml list
    fi

    printf "  ${BLUE}executing command:${NC}\n"
    printf "      conda env create \\ \n"
    printf "          --file $conda_file \\ \n"
    printf "          --prefix $conda_env_dir\n\n"

    # Create the conda environment
    conda env create \
        --file $conda_file \
        --prefix $conda_env_dir

    # Check if a matlab module has been requested
    if [ "$matlab_kernel" = true ] ; then

        # Matlab environments need the matlab engine for python to be installed
        # which needs to be done with the loaded lmod matlab module
        if [ -d "$conda_env_dir" ] ; then

            print_separator ${BLUE}
            printf "\n"
            print_separator ${BLUE}
            printf "    ${LIGHTPURPLE}The matlab python binding will be"
            printf " installed into this conda environment ${NC}\n\n"

            printf "    ${BLUE}Loading lmod modules${NC}\n"

            # Load lmod modules
            ml purge
            ml load $module_list
            ml list

            # Source the conda environment which was made, as only specific
            # versions of python are supported for the binding creation,
            # currently: 2.7, 3.5, 3.6
            source $PWD/miniconda/bin/activate $conda_env_dir

            printf "    ${BLUE}Building matlab python binding${NC}\n"

            # Build the binding
            cd $MATLAB/extern/engines/python/
            python setup.py build --build-base=$conda_env_dir/.

            engine_install_location=$conda_env_dir/lib/python3.6/site-packages/matlab

            # Remove previous installations of the binding
            if [ -d "$engine_install_location" ]; then
                rm -rf $engine_install_location/
            fi

            printf "\n    ${BLUE}Moving matlab python binding library to:\n"
            printf "        ${CYAN}$engine_install_location${NC}\n"

            # Move the binding to the proper location
            mv $conda_env_dir/lib/matlab $engine_install_location

        fi
    fi

fi

print_separator ${BLUE}
printf "\n"
