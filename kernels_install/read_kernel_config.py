"""
A python script which in the end executes docker build commands via
dockerutils.py
"""

import yaml
import sys
import argparse
import os
import json

# Make it purrty
LIGHTPURPLE = "\033[1;35m"
GREEN = "\033[0;32m"
BLUE = "\033[34m"
CYAN = "\033[1;36m"
RED = "\033[31m"
NC = "\033[0m"


def print_kernel_list(kernel_names_list):

    num_kernels = len(kernel_names_list)
    print(BLUE, "Listing kernels that can be installed:", NC)
    print(CYAN, "Number of defined kernels:", NC, len(kernel_names_list))
    for kernel_name in kernel_names_list:
        print("  ", kernel_name)


def get_kernel_info(kernel_config_file_list, kernel_name, debug):

    output_string = ''

    # The config file is expected be the same name as the kernel plus .yml
    #   e.g. the kernel maxiv-jhub-hpc-kernel-hdf5 has a config file named
    #        maxiv-jhub-hpc-kernel-hdf5.yml
    kernel_config_file = ''
    for file_in_list in kernel_config_file_list:
        if kernel_name + '.yml' in file_in_list:
            kernel_config_file = file_in_list
    if kernel_config_file != '':
        if debug:
            print('config file found')
    else:
        if debug:
            print('no config file found')
        return False

    if debug:
        print("kernel_config_file:", kernel_config_file)

    config_file_content = yaml.safe_load(open(kernel_config_file))

    if debug:
        my_json = json.dumps(config_file_content, indent=4)
        print(my_json)

    return config_file_content


def get_kernel_info_string(config_file_content):

    output_string = '"' + \
        config_file_content['kernel_name'] + '" "' + \
        config_file_content['kernel_disp_name'] + '" "' + \
        config_file_content['conda_env_name'] + '" "' + \
        config_file_content['conda_file_stable'] + '" "' + \
        config_file_content['conda_file_development'] + '" "' + \
        config_file_content['module_list'] + '" "' + \
        config_file_content['env_vars'] + '"'

    return output_string


def get_kernel_config_file_list(config_file_dir, debug):

    kernel_config_file_list = []

    # Iterate over files in given directory
    for filename in sorted(os.listdir(config_file_dir)):

        f = os.path.join(config_file_dir, filename)

        # Check if it is a yaml file
        if os.path.isfile(f) and filename.endswith('.yml'):
            if debug:
                print(f)
            kernel_config_file_list.append(f)

    if debug:
        print(kernel_config_file_list)

    return kernel_config_file_list


def get_number_kernels(kernel_config_file_list):

    num_kernels = len(kernel_config_file_list)

    return num_kernels


def get_kernel_names_list(kernel_config_file_list, debug):

    kernel_names_list = []
    for file_in_list in kernel_config_file_list:
        config_file_content = yaml.safe_load(open(file_in_list))
        kernel_names_list.append(config_file_content['kernel_name'])

    return kernel_names_list


def get_kernel_names_list_string(kernel_names_list):
    output_string = ''
    for kernel_name in kernel_names_list:
        output_string += '"' + kernel_name + '" '

    return output_string


########
# MAIN #
########

def main(argv):
    '''
    The main function - usage and help, argument parsing
    '''

    # Setup options
    parser = argparse.ArgumentParser(
        description='Get configurations listed in yaml files')
    parser.add_argument('--get_number_kernels', action='store_true',
                        help='Return the number of defined kernels')
    parser.add_argument('--print_kernel_list', action='store_true',
                        help='Print a list of the defined kernels')
    parser.add_argument('--get_kernel_names', action='store_true',
                        help='Return a list of kernel names')
    parser.add_argument('--get_kernel_info', required=False,
                        help='Return information for given kernel name'
                             ' (e.g. python3)')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='debug output')

    # Print a little extra in addition to the standard help message
    if len(argv) == 0 or '-h' in argv or '--help' in argv:
        try:
            args = parser.parse_args(['-h'])
        except SystemExit:
            print('')
            print('Examples of usage:')
            print(' python3 install_helpers/read_kernel_config.py '
                  '--print_kernel_list')
            print('')
            sys.exit()
    else:
        args = parser.parse_args(argv)

    if args.debug:
        print(args)

    # Get a list of the kernel config files
    config_file_dir = os.getcwd() + '/kernels_config/config-files/'
    kernel_config_file_list = get_kernel_config_file_list(
        config_file_dir, args.debug)

    output_string = ""

    # Get all information about a particular kernel
    if args.get_kernel_info:
        config_file_content = get_kernel_info(kernel_config_file_list,
                                              args.get_kernel_info,
                                              args.debug)
        if config_file_content:
            output_string = get_kernel_info_string(config_file_content)

    # Get the number of kernels listed in the config file
    if args.get_number_kernels:
        output_string = get_number_kernels(kernel_config_file_list)

    if args.get_kernel_names or args.print_kernel_list:
        kernel_names_list = get_kernel_names_list(kernel_config_file_list,
                                                  args.debug)

        # Get a list of the kernel names defined in the config file
        if args.get_kernel_names:
            output_string = get_kernel_names_list_string(kernel_names_list)

        # Print information about the kernels defined in the config files
        if args.print_kernel_list:
            print_kernel_list(kernel_names_list)

    if output_string:
        print(output_string)


#######################
# RUN THE APPLICATION #
#######################

if __name__ == '__main__':
    main(sys.argv[1:])
