# JupyterHub HPC - MAX IV Setup
This setup is a modification of the setup used at Aalto University:
[Aalto Triton Github Repository](https://github.com/AaltoSciComp/triton-jupyterhub)

For the original readme from Aalto University, see the section further below
named [JupyterHub for HPC clusters](jupyterhub-for-hpc-clusters). There's
useful information there.


## Installation
Installation is done with the included makefile.

![screenshot makfile help](screenshots/makefile_help.png)

There are two parts to this installation:
* JupyterHub
* JuptyerLab

The hub part can be installed on a simple machine that does not require many
resources, just slurm and a connection to the cluster nodes. It also requires
some configuration along with the installation of the apache web server.

The lab part needs to be installed in a location available to all cluster
nodes, like in a directory that gets mounted on all nodes.


### JupyterHub Installation

#### Additional Tools
On the machine which will serve as the hub - perhaps a cluster front-end or a
VM with slurm installed - some tools will need to be installed.  On Rocky8 this
is done with:
```bash
dnf install git make httpd mod_ssl
```

#### Service Account
It's probably best to do this installation with a service account with
limited permissions, in this example *jupyterhub-daemon*:
```bash
ssh hub-machine
sudo su -
groupadd -g 901 jupyterhub-daemon
useradd jupyterhub-daemon -u 901 -g 901 --system
```

#### JupyterHub Code
Choose a location for the code and then clone the repository.
```bash
sudo su - jupyterhub-daemon
umask 002
mkdir /opt/jupyterhub-hpc-hub
cd /opt/jupyterhub-hpc-hub/
git clone git@gitlab.maxiv.lu.se:scisw/jupyterhub/jupyterhub-hpc.git .
git checkout maxiv-setup
```

Now use the Makefile to install conda and the hub dependencies:
```bash
make conda
make hub-stable-env
```

#### Service Account Sudo Rights
Set up `sudoers` for the service account, which allows the account to submit
slurm jobs as other users and run the service. Do this as root:
```bash
cp service_config/jupyterhub-daemon /etc/sudoers.d/jupyterhub-daemon
chmod 440 /etc/sudoers.d/jupyterhub-daemon
```

#### JupyterHub Configuration File
If necessary, change the value of a few items:
```bash
vim jupyterhub_config.py
    --> c.JupyterHub.hub_connect_ip
    --> c.JupyterHub.base_url
    --> req_partition
    --> c.Authenticator.admin_users
    --> c.LocalAuthenticator.group_whitelist
```

Additionally, this configuration file contains a list of cluster nodes to use,
and the location of the JupyterLab installation which the nodes will have
access to, the installation of which is covered in this readme.


#### Apache Web Server Configuration
Configure Apache, as root:
```bash
cp service_config/jupyterhub.conf /etc/httpd/conf.d/jupyterhub.conf
vim /etc/httpd/conf.d/jupyterhub.conf
    --> Edit where needed:
        jupyter_domain_primary
        ProxyPass
        ProxyPassReverse
systemctl reload httpd
systemctl restart httpd
systemctl status httpd.service
```

Configure logging and log rotation for Apache, which in this example is
using the system tool "logrotate":
```bash
vim /etc/httpd/conf.d/jupyterhub.conf
    --> Edit where needed:
        ErrorLog /var/log/jupyterhub/httpd-error.log
        CustomLog /var/log/jupyterhub/httpd-access.log combined

cp service_config/jupyterhub_logrotate /etc/logrotate.d/jupyterhub
vim /etc/logrotate.d/jupyterhub
    --> Edit where needed, see "man logrotate":
        /var/log/jupyterhub/*log {
```

#### System Service Setup
For a production installation, it's probabaly best to run this as a system
service using a service account setup to do so.

Setup http proxy configuration files:
```bash
umask 0337
openssl rand -hex 32 > /etc/jupyterhub/jupyterhub_cookie_secret
chown jupyterhub-daemon:jupyterhub-daemon
    /etc/jupyterhub/jupyterhub_cookie_secret
chmod 440 /etc/jupyterhub/jupyterhub_cookie_secret

umask 0337
echo \"CONFIGPROXY_AUTH_TOKEN=`openssl rand -hex 32`\"
    > /etc/jupyterhub/configurable-http-proxy-token.var ;
chown jupyterhub-daemon:jupyterhub-daemon
    /etc/jupyterhub/configurable-http-proxy-token.var
chmod 440 /etc/jupyterhub/configurable-http-proxy-token.var

chmod 770 /etc/jupyterhub
```

Set up systemd for `jupyterhub` and `configurable-http-proxy`.
```bash
cp service_config/jupyterhub.service
    /etc/systemd/system/jupyterhub.service
chmod 0440 /etc/systemd/system/jupyterhub.service
systemctl daemon-reload
```

#### sinfo Display
There is an addition to the session selection page when starting JuptyerLab
sessions, which displays the output of sinfo. The setup of this involves
three files on the hub machine:
* jupyterhub_config.py
* hub_scripts/sinfo.py
* templates/spawn.html

Any changes involving which cluster nodes or partitions are being used, may
require changes to the above files.

To setup the display, create a link that apache will see:
```bash
mkdir /var/www/html/sinfo/
cd /var/www/html/sinfo/
ln -sf /opt/jupyterhub-hpc-dev/hub_scripts/sinfo_offline.json \
    sinfo.json
```

### JupyterLab Installation

Now choose a location which will be mounted on every node in the cluster,
then clone the repostiory, using a service account, not necessarily the same
one as used in the hub installation:

```bash
ssh lab-machine
sudo su - swadmin
umask 002
mkdir /sw/jupyterhub/jupyterhub-hpc-lab
cd /sw/jupyterhub/jupyterhub-hpc-lab
git clone git@gitlab.maxiv.lu.se:scisw/jupyterhub/jupyterhub-hpc.git .
git checkout maxiv-setup
```

Now use the Makefile to install conda and the lab dependencies:
```bash
make conda
make lab-stable-env
make link-kernels-stable
```

The last command above make links to existing kernels (see
[jupyter-kernels-apptainer](https://gitlab.maxiv.lu.se/scisw/jupyterhub/jupyter-kernels-apptainer))
in a place where the installation of JuptyerLab knows to look for kernels. In
the case of this example installation, this is simply:

```bash
ln -sf /sw/jupyterhub/jupyter-kernels-apptainer/kernel-spec-files/share/jupyter/kernels/maxiv* \
    conda/envs/lab-stable/share/jupyter/kernels/.
```

That's it for the JupyterLab installation.


### Run JupyterHub as a system service

#### Operate Service
Start, stop and restart the system service as the service account user on the
hub machine:
```bash
ssh hub-machine
sudo su - jupyterhub-daemon

cd /opt/jupyterhub-hpc-hub/
make start
make stop
make restart
```

Look at system service status and watch journal logs:
```bash
make status
make logs
```

#### Start JupyterLab Session
Once the service is running, and all firewall and proxy setup is finished, one
can start a JupyterLab session in a web browser
```bash
https://jupyterhub-hpc.maxiv.lu.se/online
```

The menu should be populated with different nodes and partitions to choose
from, along with a display of sinfo information.
![screenshot session selection](screenshots/session_selection.png)

After successfully starting a session, one can check that everything works as
expected, like all kernels being available and usable, mounted directories
available, terminals working, etc.
![screenshot jupyterlab session](screenshots/jupyterlab_session.png)


************************************************************
************************************************************

# JupyterHub for HPC clusters

This repository contains JupyterHub configuration for running it on
HPC clusters.
[Batchspawner](https://github.com/jupyterhub/batchspawner) is what
actually provides the Slurm integration, so this repository is mainly configuration and integration.  [Makefile](Makefile) supposubly automates the installation, but in practice it will only guide you in the steps you need to take.

Many uses of JupyterHub involved containers or single-user
environments.  In our model, Jupyter is not the primary tool, the cluster is
the primary tool and we try to provide an easy entry point, instead of a
replacement service.  This means *at least* we provice accesss to all the data in the
cluster, and preferably all the software, etc.


Jupyter projects from Aalto Science-IT:

* This repository which contains the actual Jupyterhub software
  installation configuration - all can be done as a normal user.

* Our ansible configuration for cluster deployment (currently minimal
  - deploys system config to CentOS7, but the actual Aalto configuration is
  seprately managed).  This does all administrator setup:
  https://github.com/AaltoScienceIT/ansible-role-fgci-jupyterhub

* User instructions: http://scicomp.aalto.fi/triton/apps/jupyter.html
  (read this to see our vision from a user's perspective).

* Jupyter for light computing/teaching using kubernetes (this repo is
  HPC jupyterhub): https://github.com/AaltoScienceIT/jupyterhub-aalto


Key dependencies from Jupyter:
* JupyterHub: https://github.com/jupyterhub/jupyterhub
* Batchspawner: https://github.com/jupyterhub/batchspawner
* Slurm config - currently not anywhere.


You may also be interested in:
* CSC Notebooks service - epheremal data only, better for small
  workshops.  https://github.com/CSCfi/pebbles



# Instructions for use

Let's say you want to use this repository to set up your own HPC
JupyterHub.  Follow these steps:

* Understand a bit about HPC and Jupyter.  These instructions aren't
  that great yet and can only serve as a guideline, not a recipe.  Be
  ready to tell us about things that don't work and contribute back.

* Clone this repository onto a shared filesystem (we use a dedicated
  virtual node, resources don't need to be large).  This becomes the
  base software installation for the hub (world-readable,
  admin-writeable).  Set up miniconda using the `install_conda` Makefile
  target.

* Install JupyterHub using the `install_all` Makefile targets.

* Create a `jupyterhub-daemon` user.

* Set up the server with HTTP server, `sudo`, etc.  This is all
  defined in
  [ansible-role-fgci-jupyterhub](https://github.com/AaltoScienceIT/ansible-role-fgci-jupyterhub).
  You can see just what happens in the [tasks
  file](https://github.com/AaltoScienceIT/ansible-role-fgci-jupyterhub/blob/master/tasks/main.yml).
  This does the following things (CentOS 7, may need changes for
  others):

  * Install and configure Apache as a frontend reverse proxy

  * Set up `sudoers` for `jupyterhub-daemon`.

  * Set up basic files in `/etc/jupyterhub`

  * Set up systemd for `jupyterhub` and `configurable-http-proxy`.

  * Set up Shibboleth web server authentication.  (Warning: shibboleth
    is magic, no promises how well this works for others).  This
    probably needs modification for non-Finnish deployments.

* Things that definitely need local tuning:

  * Installation of kernels


# Vision and design

JupyterHub should be a frontend to already-existing resources, not
seen as a separate service.  Anywhere you'd have an SSH server, have
JupyterHub too.

See our [user
instructions](http://scicomp.aalto.fi/triton/apps/jupyter.html) for
the user-centric view.


## Base environment

JupyterHub, the single-user servers, etc, are installed in a dedicated
miniconda environment.  This runs the servers, but no user code (they
all run in kernels that uses the rest of the cluster software, see
"software" below).


## Computational resources

Servers run in the Slurm queue - we have two nodes dedicated for
interactive use, so we put the servers there.  Jupyter has a very
different usage pattern than normal HPC work - CPUs are idle most of
the time, and memory usage is unknown.  Thus, these nodes are
oversubscribed in CPU (currently at least 10x).  They *should* be
oversubscribed in memory, but that is hard for our slurm right now
since globally it is a consumable resource.  Currently, this is
handled by using Slurm cgroups to limit memory: processes can go above
their requested memory by a certain amount (5x), as long as there's
enough resources available.

We provide various time vs memory tradeoffs - you can use small memory server
that will stay running a long time, or a large amonut of memory that
will be automatically culled sooner if it is inactive.  Finding the
right balances of time and memory will be an ongoing challenge.

We have "jupyter-short" and "jupyter-long" partitions, and a
"jupyter-overflow" overlapping with basic batch partitions that is
only used once the basic ones fill up.


## Spawning single-user servers

After a user logs in, they spawn their single-user server.  This code
is running as their own uid in Slurm on nodes.
[batchspawner](https://github.com/jupyterhub/batchspawner) is the
resource manager in JupyterHub and provides the interface to Slurm.
We are active developers on this.


## Software and kernels

We don't install special software for JupyterHub kernels - we have a
cluster with lots of software installed.  Instead, we provide
integration to that.  The `Makefile` has some code to automatically
install a bunch of Python kernels from Lmod, and a few more that are
automatic or semi-automatic.  The idea is you should be able to switch
between any software on the cluster easily.  Our package
[envkernel](https://github.com/NordicHPC/envkernel) helps with that.

[jupyter-lmod](https://github.com/cmd-ntrf/jupyter-lmod) provides
integration of Lmod to Jupyter Notebooks.  Using the "Softwares" tab,
you can load and unload modules and this takes effect when you start
notebooks - nice but they are global to all notebooks, and UI is a bit
confusing.


## Login

Log in is via the Jupyterhub PAM authentication module (since on
cluster nodes, PAM already works).  In the future, we hope to use
Shibboleth but as you can guess that is not trivial.  You must have a
cluster account to be able to use the service, and your JupyterHub
account *is* your cluster account.


## Web server

Apache is used as a reverse proxy for ingress, which does SSL
termination.  If you are *not* in the university network, you have to
pass Shibboleth authentication at the web server level (befor reaching
any JupyterHub code) and *then* log in with JupyterHub PAM
authentication.  This gives us enough security to allow access from
the whole Internet.


## Configuration

The `jupyterhub_config.py` file has all of important config options.

We pre-install a lot of useful extensions for users.

We use ProfileSpawner to provide a form that lets you pick what
resources you need.


## Security

According to the JupyterHub docs, when served on a single domain,
users can't be able to configure their own server (to prevent XSS and
the like).  I have carefully tracked this down and think that our
setup satisfies this.

Obviously, basic software security is very important when run on a
cluster with live data.

I have written long documentation on my security analysis which I will
paste here later.


## Using this deployment

The Ansible role can be used to set up some of the software - warning:
not very well developed.

The other installation can be somewhat done automatically using the
`Makefile`.  Yes, it's a Makefile, which doesn't suit this very well.
It's more like a shell script that you can selectively run parts of.
It should be re-done once we know where we are going.

No guarentees about anything.  This may help guide you, but you'll
need to help generalize.

Please send feedback and contribute.



# Problems in the wild

* Error reporting could be better...

* User loads modules in `.bashrc` and adjusts `$MODULEPATH`.  Loading
  some kernels that depend on certain modules no longer works and they
  keep restarting.

* nbdime is important, but needs some thought to make it available
  globally from within a conda env.  Do we install globally?  Do we
  use from within conda env?

* Not a problem, but there is a lot of potential for courses and
  workshops.  A bit of integration could make things even easier
  (e.g. spawner which copies the right base files to your working
  dir).



# Contact

Richard Darst
[email](https://people.aalto.fi/index.html?language=english#richard_darst),
Aalto University
