import sys
import json
import subprocess
from datetime import datetime


def get_sinfo_partition(cmd, opt, debug):

    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    sinfo_header = []
    sinfo_dict_list = []
    count_line = 0

    # Read output from shell command, one line at a time
    for line in proc.stdout.readlines():
        if debug:
            print("line", count_line, ":", str(line.decode()))

        count_word = 0
        word_list = str(line.decode()).split()
        sinfo_dict = {}

        # Look at each word in the line - should be 6 - should check this
        for word in word_list:
            if debug:
                print("  word", count_word, ":", word)

            # Make list of table column titles
            if count_line == 0:
                sinfo_header.append(word)
            # Save the sinfo output into a dictionary
            else:
                sinfo_dict[sinfo_header[count_word]] = word

                if count_word == len(word_list) - 1:
                    sinfo_dict_list.append(sinfo_dict)

            count_word += 1

        if debug:
            print("")

        count_line += 1

    # Check if there are any available nodes for each partition
    available_nodes = {}
    for sinfo_dict in sinfo_dict_list:
        if sinfo_dict["PARTITION"] not in available_nodes:
            available_nodes[sinfo_dict["PARTITION"]] = False
        if sinfo_dict["STATE"] == "idle":
            available_nodes[sinfo_dict["PARTITION"]] = True

    if debug:
        print("available_nodes:", available_nodes)

    # Set the color for each row
    #   - default is black
    #   - available is green
    #   - unavailable is red
    for sinfo_dict in sinfo_dict_list:
        if available_nodes[sinfo_dict["PARTITION"]]:
            sinfo_dict["COLOR"] = "black"
        else:
            sinfo_dict["COLOR"] = "red"

        if sinfo_dict["STATE"] == "idle":
            sinfo_dict["COLOR"] = "green"
        elif sinfo_dict["STATE"] == "resv" or sinfo_dict["STATE"] == "mix":
            sinfo_dict["COLOR"] = "blue"

    # Set menu option color - could be different than what is in sinfo table
    menu_option_color = {}
    for partition in available_nodes:
        if available_nodes[partition]:
            menu_option_color[opt] = "green"
        else:
            menu_option_color[opt] = "red"

    if debug:
        print("menu_option_color:", menu_option_color)
        print(sinfo_header)
        print(sinfo_dict_list)

    return sinfo_header, sinfo_dict_list, menu_option_color


def get_sinfo(location='', debug=False):
    output = {}
    sinfo_dict_list = []
    menu_dict_list = {}

    commands = []
    menu_option_names = []
    output_file_name = "hub_scripts/sinfo.json"

    if location == "online":
        commands = [
            ["sinfo", "-n", "cn[17-35]", "-p", "blades"],
            ["sinfo", "-n", "cn[36-43]", "-p", "blades"],
            ["sinfo", "-n", "cn[68-95]", "-p", "fujitsu"],
            ["sinfo", "-n", "gn[1-3]", "-p", "v100"],
            ["sinfo", "-n", "gn[4-12]", "-p", "v100"],
            ["sinfo", "-n", "cn[96-103]", "-p", "r650"]
        ]
        menu_option_names = [
            "blades_17_35",
            "blades_36_42",
            "fujitsu_68_95",
            "v100_1_3",
            "v100_4_12",
            "r650_96_103"
        ]
        output_file_name = "hub_scripts/sinfo_online.json"

    if location == "offline":
        commands = [
            ["sinfo", "-n", "offline-cn[8-15]", "-p", "all"]
        ]
        menu_option_names = [
            "all_8_15"
        ]
        output_file_name = "hub_scripts/sinfo_offline.json"

    header = None
    menu_option_color = None
    for cmd, opt in zip(commands, menu_option_names):
        header, dict_list, menu_option_color = \
            get_sinfo_partition(cmd, opt, False)
        sinfo_dict_list.extend(dict_list)
        menu_dict_list.update(menu_option_color)
    sinfo_header = header

    if debug:
        print(sinfo_dict_list)

    # Assemble output, add the current datetime
    output["datetime"] = str(datetime.now().strftime("%Y-%m-%d %H:%M"))
    output["header"] = sinfo_header
    output["sinfo"] = sinfo_dict_list
    output["menu"] = menu_dict_list

    # Write to output file
    with open(output_file_name, 'w', encoding='utf-8') as f:
        json.dump(output, f, ensure_ascii=False, indent=4)


if __name__ == "__main__":
    args = sys.argv[1:]
    if len(args) == 1:
        get_sinfo(args[0], False)
    else:
        get_sinfo()
