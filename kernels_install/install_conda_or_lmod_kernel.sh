#!/bin/bash
#
# Description: This is a script for creating conda environments, given a few
#              parameters:
#                   kernel_name
#                   kernel_disp_name
#                   yaml_file
#                   module_list

# Colors
LIGHTPURPLE="\033[1;35m"
GREEN="\033[32m"
BLUE="\033[34m"
CYAN="\033[1;36m"
RED="\033[31m"
NC="\033[0m"

# Separator between output, fills width of terminal
function print_separator
{
printf "${NC}$1%*s${NC}\n" "${COLUMNS:-80}" '' | tr ' ' '*'
}

printf "\n"
print_separator ${BLUE}

# Check input
if [[ "$#" -lt 4 ]]; then
    printf "${RED} Not enough input arguments ${NC}\n"
    print_separator ${BLUE}
    printf "\n"
    exit 0
fi

# Get input
kernel_name="$1"
kernel_disp_name="$2"
conda_env_name="$3"
yaml_file="$4"
module_list="$5"
env_vars="$6"

# See if this kerenel requires a conda environment, lmod modules, or both
if [[ "$module_list" == "none" ]]
then
    kernel_type="conda"
else
    if [[ "$yaml_file" == "none" ]]
    then
        kernel_type="lmod"
    else
        kernel_type="lmod_conda"
    fi
fi

# Some special installation steps are needed for Matlab kernels
matlab_kernel=false
if [[ $module_list == *"Matlab"* || $module_list == *"matlab"* ]]; then
    matlab_kernel=true
fi

# Source the base conda environemnt, set KERNEL_PREFIX path
source $PWD/miniconda/bin/activate $PWD/miniconda
KERNEL_PREFIX=$CONDA_PREFIX

# Set location of the conda environment, only used when creating a conda or
# lmod+conda kernel
conda_env_dir=$KERNEL_PREFIX/share/jupyter/conda_envs/$conda_env_name

# See if the kernel already exists, if so, leave it alone and exit
kernel_location="$KERNEL_PREFIX/share/jupyter/kernels/$kernel_name"
if [ -d "$kernel_location" ]
then
    printf "    ${LIGHTPURPLE}The kernel ${CYAN}$kernel_name "
    printf "${NC}${LIGHTPURPLE}already exists here:${NC}\n"
    printf "      $kernel_location \n"
    printf "    ${LIGHTPURPLE}To remove it:${NC}\n"
    printf "        make clean-kernel-dev/$kernel_name\n"
    printf "    ${LIGHTPURPLE}exiting${NC}\n"
    print_separator ${BLUE}
    printf "\n"
    exit 0
else
    case "$kernel_type" in
      "conda")
        printf "    ${CYAN}Installing a conda environment kernel${NC}\n"
        ;;
      "lmod")
        printf "    ${CYAN}Installing an lmod kernel${NC}\n"
        ;;
      "lmod_conda")
        printf "    ${CYAN}Installing combined conda + lmod kernel${NC}\n"
        ;;
    esac
    printf "    ${CYAN}         name: ${NC}$kernel_name${NC}\n"
    printf "    ${CYAN}   conda name: ${NC}$conda_env_name${NC}\n"
    printf "    ${CYAN}  kernel type: ${NC}$kernel_type${NC}\n"
    printf "    ${CYAN} display name: ${NC}$kernel_disp_name${NC}\n"
    printf "    ${CYAN}  module_list: ${NC}$module_list${NC}\n"
    printf "    ${CYAN}     env_vars: ${NC}$env_vars{NC}\n"
    if [[ "$kernel_type" = "conda" || "$kernel_type" = "lmod_conda" ]]
    then
        printf "    ${CYAN}    conda env: ${NC}$conda_env_dir${NC}\n"
    fi
    printf "\n"

    # Check that the conda environment is available, if needed
    if [[ "$kernel_type" = "conda" || "$kernel_type" = "lmod_conda" ]]
    then
        printf "    ${BLUE}Verifying that the conda environment directory "
        printf "exists${NC}\n"
        if [ -d "$conda_env_dir" ]
        then
            printf "      ${GREEN}--> Conda environment directory exists, "
            printf "continuing${NC}\n"
        else
            printf "      ${RED}--> Conda environment does not exist, "
            printf "exiting${NC}\n"
            print_separator ${BLUE}
            printf "\n"
            exit 0
        fi
        printf "\n"
    fi

    # Check that the lmod modules are available, if needed
    if [[ "$kernel_type" = "lmod" || "$kernel_type" = "lmod_conda" ]]
    then
        printf "    ${BLUE}Verifying that it is possible to load the given "
        printf "lmod modules${NC}\n"

        # Set environmental variable in case it's needed to load the modules
        if [[ "$env_vars" != "none" ]]
        then
            export $env_vars
        fi

        ml purge
        ml load $module_list
        ml list
    fi

    # Create the kernel
    # 	- First with the "ipython kernel" command
    #	- Then with the "envkernel <mode>" command, where mode is either
    # 	  lmod or conda
    # 	- For combined lmod+conda kernels, the envkernel command needs to be
    #	  run twice, with the conda mode coming first
    printf "    ${BLUE}Installing $kernel_type kernel $kernel_name into: ${NC}\n"
    printf "        $KERNEL_PREFIX/share/jupyter/kernels/$kernel_name\n"
    printf "\n"

    # Assemble the initial command - ipython for most except matlab kernels
    cmd_initial=""
    if [ "$matlab_kernel" = true ] ; then
        # cmd_initial+="python -m matlab_kernel install "
        cmd_initial+="$conda_env_dir/bin/python -m matlab_kernel install "
    else
        cmd_initial+="ipython kernel install "
    fi
    cmd_initial+="--name=$kernel_name "
    cmd_initial+="--prefix=$KERNEL_PREFIX/ "

    step_count=1
    printf "        ${BLUE}Step $step_count: ${NC}\"ipython kernel\" command:\n"
    printf "            $cmd_initial\n"
    printf "\n"

    # Source the conda environment which includes matlab_kernel
    if [ "$matlab_kernel" = true ] ; then
        source $PWD/miniconda/bin/activate $conda_env_dir
    fi

    # Execute the inital kernel command
    bash -c "$cmd_initial"

    if [ "$matlab_kernel" = true ] ; then
        conda deactivate
    fi
    printf "\n"

    # Assemble the envkernel command(s), then execute
    if [[ $kernel_type = "conda" || $kernel_type = "lmod_conda" ]]
    then
        cmd_evkernel=""
        cmd_evkernel+="envkernel conda "
        cmd_evkernel+="--name=$kernel_name "
        cmd_evkernel+="--kernel-template=$kernel_name "
        cmd_evkernel+="$conda_env_dir "
        cmd_evkernel+="--display-name=\"$kernel_disp_name\" "
        cmd_evkernel+="--prefix=$KERNEL_PREFIX/ "

        # For combined matlab kernels, this option should not be used
        if [ "$matlab_kernel" = false ] ; then
            cmd_evkernel+="--python=python "
        fi

        # Add environmental variable to the kernel
        if [[ $kernel_type = "conda" && "$env_vars" != "none" ]]
        then
            # The env_var may contain multiple environmental variables
            # seperated by spaces, e.g.:
            #  "HDF5_PATH=/path/to/blah CONDA_PATH=/path/to/bleh"
            for env_var in $env_vars
            do
                # The --env argument may be given multiple times to envkernel
                cmd_evkernel+="--env=$env_var "
            done
        fi

        step_count=$((step_count + 1))
        printf "        ${BLUE}Step $step_count: ${NC}\"envkernel\" command:\n"
        printf "            $cmd_evkernel\n"
        printf "\n"

        # Execute the envkernel command
        bash -c "$cmd_evkernel"
        printf "\n"
    fi

    if [[ $kernel_type = "lmod" || $kernel_type = "lmod_conda" ]]
    then
        cmd_evkernel=""
        cmd_evkernel+="envkernel lmod "
        cmd_evkernel+="--name=$kernel_name "
        cmd_evkernel+="--kernel-template=$kernel_name "
        cmd_evkernel+="--purge "
        cmd_evkernel+="$module_list "
        cmd_evkernel+="--display-name=\"$kernel_disp_name\" "
        cmd_evkernel+="--prefix=$KERNEL_PREFIX/ "

        # For combined conda+lmod kernels, this option should not be used
        if [[ $kernel_type = "lmod" ]] ; then
            cmd_evkernel+="--python=python "
        fi

        # Add environmental variable to the kernel
        if [[ "$env_vars" != "none" ]]
        then
            # The env_var may contain multiple environmental variables
            # seperated by spaces, e.g.:
            #  "HDF5_PATH=/path/to/blah CONDA_PATH=/path/to/bleh"
            for env_var in $env_vars
            do
                # The --env argument may be given multiple times to envkernel
                cmd_evkernel+="--env=$env_var "
            done
        fi

        step_count=$((step_count + 1))
        printf "        ${BLUE}Step $step_count: ${NC}\"envkernel\" command:\n"
        printf "            $cmd_evkernel\n"
        printf "\n"

        # Execute the envkernel command
        bash -c "$cmd_evkernel"
        printf "\n"
    fi

fi

print_separator ${BLUE}
printf "\n"
