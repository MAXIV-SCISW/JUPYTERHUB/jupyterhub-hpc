# Configuration file for jupyterhub.

import os
import sys
# Must import batchspawner! Even if not explicitly used in this file.
import batchspawner  # noqa

# Read environmental variables - used for determining dev or stable version,
# as well as which front-end is being used.
stable = 'DEV' not in os.environ
dev = 0 if not os.environ.get('DEV', '').isdigit() else int(os.environ['DEV'])
location = False if not os.environ.get('LOCATION', '') \
    else os.environ['LOCATION']
print("dev:                 ", dev)
print("stable:              ", stable)
print("location:            ", location)

BASEDIRHUB = os.path.dirname(__file__)
BASEDIRNODE = '/sw/jupyterhub/jupyterhub-hpc-node'
LABENV = 'lab-stable'


################
# HUB SETTINGS #
################

c = get_config()  # noqa

# Set the hub ip address, listen on all interfaces
c.JupyterHub.ip = '0.0.0.0'
c.JupyterHub.hub_ip = '0.0.0.0'
c.JupyterHub.hub_connect_ip = ''
# IP as seen on the network. Can also be a hostname.
if location == "online":
    c.JupyterHub.hub_connect_ip = 'b-v-jupyterhub-hpc-0'
if location == "offline":
    c.JupyterHub.hub_connect_ip = 'clu1-fe-1'

print("c.JupyterHub.hub_ip: ", c.JupyterHub.hub_ip)
print("c.JupyterHub.hub_connect_ip: ", c.JupyterHub.hub_connect_ip)

port_offset = 0
c.JupyterHub.base_url = '/' + location
jupyter_system_etc_dir = '/etc/jupyterhub/'

# Running stable (production mode)
if stable:
    port_offset = 0
    c.JupyterHub.base_url = '/' + location
    jupyter_system_etc_dir = '/etc/jupyterhub/'
    LABENV = 'lab-stable'

# Running in development mode
# Different ports and url, different location for proxy and cookie files
else:
    port_offset = 200
    c.JupyterHub.base_url = '/dev' + location
    jupyter_system_etc_dir = '/etc/jupyterhub/dev/'
    LABENV = 'lab-dev'

# System config files
c.JupyterHub.db_url = 'sqlite:///' + jupyter_system_etc_dir + \
    'jupyterhub.sqlite'
c.JupyterHub.cookie_secret_file = jupyter_system_etc_dir + \
    'jupyterhub_cookie_secret'
c.ConfigurableHTTPProxy.auth_token = \
    open(jupyter_system_etc_dir + 'configurable-http-proxy-token.var',
         mode='r').readline().split('=', 1)[1].strip()

# Ports & proxy
c.JupyterHub.hub_port = 8081 + port_offset
c.JupyterHub.port = 8000 + port_offset
c.ConfigurableHTTPProxy.api_url = 'http://127.0.0.1:%s' % (8001 + port_offset)
c.ConfigurableHTTPProxy.should_start = True
c.ConfigurableHTTPProxy.pid_file = 'jupyterhub-proxy-' + \
    c.JupyterHub.hub_connect_ip + '-' + location + '-' + \
    str(c.JupyterHub.port) + '.pid'
c.ConfigurableHTTPProxy.log_level = "error"

# Prevents servers from being killed
if 'JUPYTER_PERSIST_ACROSS_RESTARTS' in os.environ:
    c.JupyterHub.cleanup_servers = False

# Look and feel
c.JupyterHub.template_paths = [os.path.join(BASEDIRHUB, 'templates')]


###########
# LOGGING #
###########
#  Set the log level by value or name.
#  Choices: any of [0, 10, 20, 30, 40, 50,
#                   'DEBUG', 'INFO', 'WARN', 'ERROR', 'CRITICAL']
#  Default: 30 == 'WARN'
#  'DEBUG' is equivelant to log_level=10 and using option --debug
c.JupyterHub.log_level = 'INFO'


##################
# AUTHENTICATION #
##################

c.Authenticator.allow_all = True

# Admin users and allowed groups
c.Authenticator.admin_users = set()
c.LocalAuthenticator.group_whitelist = {'MAX-Lab', 'Visitors'}
# security-related
c.Spawner.disable_user_config = True
c.JupyterHub.cookie_max_age_days = 3
# Depreciated setting cookie_options?:
# c.JupyterHub.cookie_options = dict(secure=True)


############
# SERVICES #
############
c.JupyterHub.services = [
    {
        'name': 'sinfo',
        'admin': True,
        'command': [sys.executable, 'hub_scripts/sinfo.py', location],
    }
]
c.JupyterHub.load_roles = [
    {
        'name': 'server',
        'scopes': ['inherit'],
    }
]

###########
# SPAWNER #
###########

c.JupyterHub.spawner_class = 'wrapspawner.ProfilesSpawner'
c.SlurmSpawner.batchspawner_singleuser_cmd = \
    '{BASEDIRNODE}/conda/envs/{LABENV}/bin/batchspawner-singleuser'.format(
        BASEDIRNODE=BASEDIRNODE, LABENV=LABENV)

c.Spawner.http_timeout = 180

# Slurm options can be found at
#  https://github.com/jupyterhub/batchspawner/blob/master/batchspawner/
#    batchspawner.py#L78
# Environmental variables can be set in the prologue
prologue = """\
set -x
PATH={CONDA_PREFIX}/bin:$PATH
echo $PATH
export JUPYTER_PATH=$SNIC_TMP/jupyter/
export HOME=/home/$USER
export APPTAINER_BINDPATH="/mxn,/data/staff,/data/visitors,/data/proprietary,/home,$SNIC_TMP/pip:/opt/pip"
echo $HOME
export CONDA_ENVS_PATH=/home/$USER/.conda/envs
unset XDG_RUNTIME_DIR
sh {BASEDIRNODE}/hub_scripts/setup_tree.sh
""".format(BASEDIRNODE=BASEDIRNODE,
           CONDA_PREFIX=BASEDIRNODE + '/conda/envs/' + LABENV)

cmd = "{BASEDIRNODE}/conda/envs/{LABENV}/bin/jupyter-labhub "\
      "--FileContentsManager.delete_to_trash=False".format(
          BASEDIRNODE=BASEDIRNODE, LABENV=LABENV)

slurm_default = dict(req_partition='interactive',
                     req_workdir='/home/{user}',
                     cmd=cmd,
                     req_prologue=prologue,
                     )

# Selection criteria for different nodes - depends on the cluster being used
if location == "online":
    profiles = [
        ["CPU node:  24 CPU cores, 128 GB RAM, 12 hours \
            (partition: blades, nodes: cn[17-35])",
         'blades_17_35', 'batchspawner.SlurmSpawner',
         {**slurm_default,
          **dict(req_srun='',
                 req_partition='blades',
                 req_options='-N 1 -n 24 -c 2 -C mem128GB --mem-per-cpu 2291',
                 req_runtime='0-12')}],

        ["CPU node:  24 CPU cores, 256 GB RAM, 12 hours \
            (partition: blades, nodes: cn[36-42])",
         'blades_36_42', 'batchspawner.SlurmSpawner',
         {**slurm_default,
          **dict(req_srun='',
                 req_partition='blades',
                 req_options='-N 1 -n 24 -c 2 -C mem256GB --mem-per-cpu 4583',
                 req_runtime='0-12')}],

        ["CPU node:  32 CPU cores, 384 GB RAM, 12 hours \
            (partition: fujitsu, nodes: cn[68-95])",
         'fujitsu_68_95', 'batchspawner.SlurmSpawner',
         {**slurm_default,
          **dict(req_srun='',
                 req_partition='fujitsu',
                 req_options='-N 1 -n 32 -c 2 -C mem384GB --mem-per-cpu 5468',
                 req_runtime='0-12')}],

        ["GPU node:   4 GPUs, 32 CPU cores, 192 GB RAM, 12 hours \
            (partition: v100, nodes: gn[1-3])",
         'v100_1_3', 'batchspawner.SlurmSpawner',
         {**slurm_default,
          **dict(req_srun='',
                 req_partition='v100',
                 req_ngpus='4',
                 req_options='-N 1 -n 32 -c 2 -C mem192GB --mem=0',
                 req_runtime='0-12')}],

        ["GPU node:   4 GPUs, 36 CPU cores, 384 GB RAM, 12 hours \
            (partition: v100, nodes: gn[4-12])",
         'v100_4_12', 'batchspawner.SlurmSpawner',
         {**slurm_default,
          **dict(req_srun='',
                 req_partition='v100',
                 req_ngpus='4',
                 req_options='-N 1 -n 36 -c 2 -C mem384GB --mem=0',
                 req_runtime='0-12')}],

        ["CPU node:  48 CPU cores, 512 GB RAM, 12 hours \
            (partition: r650, nodes: cn[96-103])",
         'r650_96_103', 'batchspawner.SlurmSpawner',
         {**slurm_default,
          **dict(req_srun='',
                 req_partition='r650',
                 req_options='-N 1 -n 48 -c 2 --mem=0',
                 req_runtime='0-12')}],

    ]

if location == "offline":
    profiles = [
        ["CPU node:  24 CPU cores, 128 GB RAM, 12 hours \
            (partition: all, nodes: offline-cn[8-15], Rocky8)",
         'all_8_15', 'batchspawner.SlurmSpawner',
         {**slurm_default,
          **dict(req_srun='',
                 req_partition='all',
                 req_options='-N 1 -n 24 -c 2 -C mem128GB --mem-per-cpu 2291',
                 req_runtime='0-12')}],
    ]

c.ProfilesSpawner.profiles = profiles

# visible filesystem tree
c.Spawner.notebook_dir = '/home/{username}/.jupyterhub-tree/'
c.Spawner.default_url = '/lab'

c.Spawner.poll_interval = 300
c.Spawner.start_timeout = 240  # timeout upon startup (slurm queue time)
c.JupyterHub.last_activity_interval = 300

# Allow metrics to be published without authentication by setting this to False
c.JupyterHub.authenticate_prometheus = False
