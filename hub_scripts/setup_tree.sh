#!/bin/bash -l

# Create Jupyter home tree
JHOME=$HOME/.jupyterhub-tree
mkdir -p $JHOME
chmod u+w $JHOME

# Add some useful links - modified for MAXIV setup

# User home directories on the cluster
mkdir -p $JHOME/home
test -e $JHOME/home/$USER    || ln -sT $HOME $JHOME/home/$USER

# Data directories
test -e $JHOME/data    || ln -sT /data $JHOME/data

# Shared directories
mkdir -p $JHOME/mxn
test -e $JHOME/mxn/groups     || ln -sT /mxn/groups $JHOME/mxn/groups

# User home directories in the storage - typical home directories
if [ $SLURM_JOB_ACCOUNT == "visitors" ];
then 
    mkdir -p $JHOME/mxn/visitors
    test -e $JHOME/mxn/visitors/$USER || ln -sT /mxn/visitors/$USER $JHOME/mxn/visitors/$USER

    # Remove directories and links made previously by mistake
    if [ -L $JHOME/mxn/home/$USER ];
    then
        rm $JHOME/mxn/home/$USER
    fi
    if [ -d $JHOME/mxn/home ];
    then
        rmdir $JHOME/mxn/home
    fi        
else
    mkdir -p $JHOME/mxn/home
    test -e $JHOME/mxn/home/$USER || ln -sT /mxn/$HOME $JHOME/mxn/home/$USER
fi

# Make this directory non-modifiable for others
chmod u+w,g-w,o-w $JHOME

# Also create the directory into which user made conda files are to be placed
CONDA_ENVS_PATH=$HOME/.conda/envs
mkdir -p $CONDA_ENVS_PATH

# Dumb fix for user kernels not being found by jupyter
# Also need to set JUPYTER_PATH=$SNIC_TMP/jupyter/ in prologue in
# jupyterhub_config.py on the hub machine
mkdir -p $SNIC_TMP/jupyter
mkdir -p $HOME/.local/share/jupyter/kernels
ln -sf $HOME/.local/share/jupyter/kernels $SNIC_TMP/jupyter/.

# Dumb fix 2 - create directory into which users can temporarily install modules
# with pip
mkdir -p $SNIC_TMP/pip

# Clean up old spawner logfiles
find ~/ -maxdepth 1 -regextype egrep -regex '.*jupyterhub_slurmspawner_[0-9]+.log' -mtime '+7' -delete
